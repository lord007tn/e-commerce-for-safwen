import React from 'react'

const ContactScreen = () => {
    return (
        <div>
            <div class="container">
  <div style={{textAlign: "center"}}>
    <h2>Contact Us</h2>
    <p>Swing by for a cup of coffee, or leave us a message:</p>
  </div>
  <div class="row">
    <div class="column">
      <img src="https://via.placeholder.com/450*600" style={{width: "100%"}}/>
    </div>
    <div class="column">
      <form>
        <label for="fname">First Name</label>
        <input type="text" id="fname" name="firstname" placeholder="Your name.."/>
        <label for="lname">Last Name</label>
        <input type="text" id="lname" name="lastname" placeholder="Your last name.."/>
        <label for="subject">Subject</label>
        <textarea id="subject" name="subject" placeholder="Write something.." style={{height: "170px"}}></textarea>
        <button type="submit" className="button primary">Submit</button>
      </form>
    </div>
  </div>
</div>
        </div>
    )
}

export default ContactScreen
